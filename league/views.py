from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from league.models import Customer, LeagueUser, Point, Season

def purchase(request):
	customer = get_object_or_404(Customer, pk=request.POST.get('customer'))
	user = get_object_or_404(LeagueUser, pk=request.POST.get('user'))
	points = int(request.POST.get('points'))

	Point.objects.create(source=customer, points=points, user=user)
	season = Season.get_current_season(user)
	season.points += points
	season.save()
	return HttpResponse('ok')