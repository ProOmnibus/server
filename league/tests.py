from datetime import datetime, timedelta

from django.test import TestCase
from django.contrib.auth.models import User

from league.models import Customer, LeagueUser, Season


class TestLeague(TestCase):

	def setUp(self):
		u1 = User.objects.create(username='User1', password='password1',
			email='user1@example.com')
		self.user1 = LeagueUser.objects.get(user=u1)
		self.customer1 = Customer.objects.create(name='Customer1')
		self.current_season = Season.objects.create(
			start=datetime.now() - timedelta(days=1), user=self.user1)

	def testStoreSwipe(self):
		r = self.client.post('/league/purchase/', {
			'user': self.user1.pk,
			'customer': self.customer1.pk,
			'points': 10})

		self.assertContains(r, 'ok', count=1, status_code=200)
		self.assertEquals(10, self.user1.current_season().points)