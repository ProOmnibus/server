from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

USERS_PER_LEAGUE = 20


class Certification(models.Model):
	short_description = models.CharField(max_length=255)
	long_description = models.TextField()
	valid = models.DateField()


class Customer(models.Model):
	name = models.CharField(max_length=255)
	certifications = models.ManyToManyField('Certification')


class LeagueUser(models.Model):
	modifiers = models.ManyToManyField('Modifiers')
	user = models.OneToOneField(User)

	def __unicode__(self):
		return '%s\'s LeagueUser' % str(self.user)

	def current_season(self):
		return Season.get_current_season(self)

@receiver(post_save, sender=User)
def create_voter(sender, instance, **kwargs):
    LeagueUser.objects.get_or_create(user=instance)


class Modifiers(models.Model):
	short_description = models.CharField(max_length=250)
	long_description = models.TextField(blank=True, null=True)
	modifier = models.IntegerField(default=0)

	def __unicode__(self):
		return self.short_description


class Point(models.Model):
	date = models.DateTimeField(default=datetime.now())
	source = models.ForeignKey('Customer')
	points = models.IntegerField(default=0)
	user = models.ForeignKey('LeagueUser')

	def __unicode__(self):
		return '%s - %s - %s' % (self.date, self.points, self.source)


class Season(models.Model):
	start = models.DateField()
	points = models.IntegerField(default=0)
	user = models.ForeignKey('LeagueUser')

	@staticmethod
	def get_current_season(user):
		return Season.objects.filter(user=user, start__lte=datetime.now())[0]
