from django.contrib import admin

from voting.models import Campaign, Proposition, Voter

# Re-register UserAdmin
admin.site.register(Campaign)
admin.site.register(Proposition)
admin.site.register(Voter)