from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.db.utils import IntegrityError
from django.dispatch import receiver


NO_OPINION = 100
NOT_VOTED_ON = 101
COMPLETE_CHOICES = zip(range(0, 99), range(0, 99))
COMPLETE_CHOICES.append((NO_OPINION, 'NO OPINION'))
COMPLETE_CHOICES.append((NOT_VOTED_ON, 'Not voted on'))


class Voter(models.Model):
    campaigns = models.ManyToManyField('Campaign',
        related_name='individual_campaign', blank=True, null=True)
    user = models.OneToOneField(User)

    def __unicode__(self):
        return str(self.user)


@receiver(post_save, sender=User)
def create_voter(sender, instance, **kwargs):
    Voter.objects.get_or_create(user=instance)


class Proposition(models.Model):

    campaign = models.ForeignKey('Campaign')
    link = models.URLField(blank=True, null=True)
    long_description = models.TextField(blank=True, null=True)
    short_description = models.CharField(max_length=100)
    sum_score = models.IntegerField(default=0)
    total_votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.short_description

    def get_all_votes(self):
        return Vote.objects.filter(proposition=self)

    def register_vote(self, vote):
        if self.campaign.polls_are_open():

            vote.proposition = self
            vote.save()
            self.sum_score += vote.rating
            self.total_votes += 1
            self.save()
        else:
            raise Exception('You cannot vote on that campaign now')

    def save(self, *args, **kwargs):
        super(Proposition, self).save(*args, **kwargs)

@receiver(post_save, sender=Proposition)
def create_votes(sender, instance, **kwargs):
    for voter in Voter.objects.filter(campaigns__pk=instance.campaign.pk):
        try:
            Vote.objects.get_or_create(proposition=instance, voter=voter,
                vote_date=datetime.now())
        except IntegrityError:
            pass


class Campaign(models.Model):

    start_time = models.DateTimeField()
    polls_open = models.DateTimeField()
    end_time = models.DateTimeField()
    short_description = models.CharField(max_length=100)

    def __unicode__(self):
        return self.short_description

    def accepting_propositions(self):
        if self.start_time < datetime.now() < self.polls_open:
            return True
        return False

    def add_proposition(self, proposition):
        if self.accepting_propositions():
            proposition.campaign = self
            proposition.save()
        else:
            raise Exception('You cannot add propositions at this time')

    def is_closed(self):
        if datetime.now() > self.end_time:
            return True
        else:
            return False

    def get_all_propositions(self):
        return Proposition.objects.filter(campaign=self)

    def polls_are_open(self):
        if self.polls_open < datetime.now() < self.end_time:
            return True
        return False


class Vote(models.Model):

    rating = models.IntegerField(choices=COMPLETE_CHOICES, default=NOT_VOTED_ON)
    proposition = models.ForeignKey('Proposition')
    vote_date = models.DateTimeField()
    voter = models.ForeignKey(Voter)

    class Meta:
        unique_together = (('proposition', 'voter'),)

    def __unicode__(self):
        return '%s rated %s at %s' % (self.voter, self.proposition, self.get_rating_display())