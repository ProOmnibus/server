from django.conf.urls import * #@UnusedWildImport


urlpatterns = patterns('voting.views',
    (r'^campaign/(?P<pk>[-\w]+)/$', 'campaign'),
    (r'^campaign/(?P<pk>[-\w]+)/vote/$', 'post_vote'),
    (r'^campaigns/$', 'all_campaigns'),
    (r'^proposition/(?P<pk>[-\w]+)/$', 'proposition'),
    (r'^$', 'all_campaigns'),
)