from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template.context import RequestContext
from django.views.decorators.cache import never_cache

from voting.models import Campaign, Proposition, Vote, NOT_VOTED_ON


def all_campaigns(request):
    campaigns = Campaign.objects.all()
    return render_to_response('voting/all_campaigns.html', locals(),
        context_instance=RequestContext(request))


@login_required
@never_cache
def campaign(request, pk):
    campaign = get_object_or_404(Campaign, pk=pk)
    votes = Vote.objects.filter(voter=request.user.voter)
    return render_to_response('voting/campaign.html', locals(),
        context_instance=RequestContext(request))


@login_required
def post_vote(request, pk):
    if request.method == 'POST':
        for key, rating in request.POST.items():
            key = key.split('_')
            if key[0] == 'id':
                vote = Vote.objects.get(voter=request.user.voter,
                    pk=key[1])
                vote.rating = int(rating)
                vote.vote_date = datetime.now()
                vote.save()
    return render_to_response('voting/thanks.html')


@login_required
def proposition(request, pk):
    proposition = get_object_or_404(Proposition, pk=pk)
    return render_to_response('voting/proposition.html', locals(),
        context_instance=RequestContext(request))


@login_required
def user_campaigns(request):
    campaigns = Campaign.objects.all()
    return render_to_response('voting/user_campaigns.html', locals(),
        context_instance=RequestContext(request))


@login_required
def votes(request):
    votes = Vote.objects.filter(voter=request.user)
    return render_to_response('voting/votes.html', locals(),
        context_instance=RequestContext(request))